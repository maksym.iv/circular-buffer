class CircularBuffer {
  constructor(length) {
    this.length = length;
    this.buffer = new Array(length);
    this.size = 0;
    this.next = 0;
    this.last = 0;
  }

  isFull() {
    return this.size === this.length;
  }

  read() {
    if (this.size === 0) throw BufferEmptyError;
    const res = this.buffer[this.last];
    this.last = (this.last + 1) % this.length;
    this.size--;
    return res;
  }

  write(value) {
    if (!value) return;
    if (this.isFull()) throw BufferFullError;
    this.buffer[this.next] = value;
    this.next = (this.next + 1) % this.length;
    this.size++;
  }

  forceWrite(value, toReplace) {
    if (!this.isFull()) {
      this.write(value);
    } else {
      const index = this.buffer.indexOf(toReplace);
      if (index !== -1) {
        this.buffer[index - 1] = value;
        this.next = (index) % this.length;
        this.last = (index) % this.length;
      } else {
        this.buffer[this.next] = value;
        this.next = (this.next + 1) % this.length;
        this.last = (this.last + 1) % this.length;
      }
    }
  }

  clear() {
    this.size = 0;
    this.next = 0;
    this.last = 0;
    this.buffer = [];
  }
}

const circularBuffer = length => new CircularBuffer(length);

export const BufferFullError = new Error('Buffer full!');
export const BufferEmptyError = new Error('Buffer empty!');
export default circularBuffer;