import circularBuffer, { BufferFullError, BufferEmptyError } from './circular-buffer';

describe('CircularBuffer', () => {
  test('Case 1 = reading an empty buffer', () => {
    const buffer = circularBuffer(7);
    expect(() => buffer.read()).toThrow(BufferEmptyError);
  });

  test('Case 2 = write and read one item', () => {
    const buffer = circularBuffer(7);
    buffer.write('1');
    expect(buffer.read()).toBe('1');
    expect(() => buffer.read()).toThrow(BufferEmptyError);
  });

  test('Case 3 = added two more elements after the 1', () => {
    const buffer = circularBuffer(7);
    buffer.write('1');
    buffer.read();
    buffer.write('2');
    buffer.write('3');
    expect(buffer.read()).toBe('2');
    expect(buffer.read()).toBe('3');
  });

  test('Case 4 = two elements are then removed, leaved the buffer with just a 3', () => {
    const buffer = circularBuffer(7);
    buffer.write('1');
    buffer.write('2');
    buffer.clear();
    expect(() => buffer.read()).toThrow(BufferEmptyError);
    buffer.write('3');
    expect(buffer.read()).toBe('3');
    expect(() => buffer.read()).toThrow(BufferEmptyError);
  });

  test('Case 5 = writing to a full buffer an throw error that the buffer is full', () => {
    const buffer = circularBuffer(7);
    buffer.write('6');
    buffer.write('7');
    buffer.write('8');
    buffer.write('9');
    buffer.write('3');
    buffer.write('4');
    buffer.write('5');
    expect(() => buffer.write('1')).toThrow(BufferFullError);
  });

  test('Case 6 = buffer is full, overwrite the oldest data with a forced write', () => {
    const buffer = circularBuffer(7);
    ['6', '7', '8', '9', '3', '4', '5'].map(num => buffer.write(num));
    buffer.forceWrite('A', '4');
    buffer.forceWrite('B');
    expect(buffer.buffer).toEqual(['6', '7', '8', '9', 'A', 'B', '5']);
  });

  test('Case 7 & 8 = force replace 3 & 4 to A, B. Replace next values without force write', () => {
    const buffer = circularBuffer(7);
    ['6', '7', '8', '9', '3', '4', '5'].map(num => buffer.write(num));
    buffer.forceWrite('A', '4');
    buffer.forceWrite('B');
    expect(buffer.read()).toBe('5');
    expect(buffer.read()).toBe('6');
    buffer.read();
    buffer.read();
    buffer.write('C');
    buffer.write('D');
    expect(buffer.buffer).toEqual(['D', '7', '8', '9', 'A', 'B', 'C'])
  });
});